#include "tab_trous.h"

TableauATrous* create(int szm) {
    // Crée et renvoit le pointeur sur un tableau a trous de taille szm.
    TableauATrous* table = malloc(sizeof(TableauATrous));
    table->data = malloc(szm * sizeof(double));
    table->trous = malloc(szm * sizeof(char));
    table->tailleMax = szm;
    table->nbElements = 0;
    table->indicePremiereDonnee = 0;
    for(int i = 0; i < szm; i++) {
        table->trous[i] = 'T';
    }
    return table;
}

void destroy(TableauATrous* table) {
    // Détruit (libère la mémoire) du tableau à trous.
    free(table->data);
    free(table->trous);
    free(table);
}

bool is_empty(TableauATrous* table) {
    // Renvoit vrai si la structure ne contient aucune donnée.
    return table->nbElements == 0;
}

int is_full(TableauATrous* table) {
    // Renvoit vrai si toutes les cases sont occupées.
    return table->nbElements == table->tailleMax;
}

int get_first_index(TableauATrous* table) {
    // Renvoit l’indice du premier élement du tableau.
    for(int i = 0; i < table->tailleMax; i++) {
        if (table->trous[i] == 'D') {
            return i;
        }
    }
}

int get_next_index(TableauATrous* table, int index) {
    // Renvoit l’indice de l’élément suivant celui d’indice index.
    for(int i = index + 1; i < table->tailleMax; i++) {
        if (table->trous[i] == 'D') {
            return i;
        }
    }
}

void print(TableauATrous* table) {
    // Affiche le tableau avec le numéros de cases utilisé. On affichera aussi la chaine des trous.
    printf("========================\n");
    printf("Nb = %d\n", table->nbElements);
    printf("Trous : ");
    for (int i = 0; i < table->tailleMax; i++) {
        printf("%c", table->trous[i]);
    }
    printf("\n");
    if(is_empty(table)){
        printf("Tableau vide !\n");
        return;
    }
    for (int i = 0; i < table->tailleMax; i++) {
        if(table->trous[i] == 'D'){
            printf("Data [ %d ] = %lf\n", i, table->data[i]);
        }
    }
    printf("________________________\n");
}

void clear(TableauATrous* table) {
    // Nettoie le tableau (Même état que lorsqu’il a été créé).
    table->data = malloc(table->tailleMax * sizeof(double));
    table->trous = malloc(table->tailleMax * sizeof(char));
    for(int i = 0; i < table->tailleMax; i++) {
        table->trous[i] = 'T';
    }
    table->nbElements = 0;
    table->indicePremiereDonnee = 0;
}

int insert(TableauATrous* table, double val) {
    // Insere un élément dans une case libre et renvoit son index. Si l’insertion n’est pas possible -1.
    if (is_full(table)) {
        return -1;
    } else {
        int index = 0;
        while (table->trous[index] == 'D') {
            index++;
        }
        table->data[index] = val;
        table->trous[index] = 'D';
        table->nbElements++;
        return index;
    }
}

double get_value(TableauATrous* table, int index) {
    // Renvoit la valeur contenu à l’index donné.
    return table->data[index];
}

void set_value(TableauATrous* table, int index, double value) {
    // Affecte une valeur à une case tableau. Si la case était ocuppé le contenu est écrasé.
    if (index > table->tailleMax || index < 0) {
        return;
    }
    table->data[index] = value;
    table->trous[index] = 'D';
    table->nbElements++;
}

int release(TableauATrous* table, int index) {
    // Libère la case du tableau à l’index donné. Si la case était déjà libre, rien ne se passe.
    if (index > table->tailleMax || index < 0) {
        return -1;
    }
    table->trous[index] = 'T';
    table->data[index] = 0;
    table->nbElements--;
    return 0;
}

int find_first_value(TableauATrous* table, double val) {
    // Recherche une valeur et renvoit son adresse.
    for (int i = 0; i < table->tailleMax; i++) {
        if (table->data[i] == val) {
            return i;
        }
    }
    return -1;
}

int find_nb_occurences(TableauATrous* table, double value) {
    // Recherche le nombre d’occurence une valeur.
    int nb = 0;
    for (int i = 0; i < table->tailleMax; i++) {
        if (table->data[i] == value) {
            nb++;
        }
    }
    return nb;
}

void shrink(TableauATrous* table) {
    // Compacte le tableau, et fait en sorte que la mémoire allouée soit le minimum nécessaire au
    //stockage des données au moment de l’appel.
    int nb = 0;
    for (int i = 0; i < table->tailleMax; i++) {
        if (table->trous[i] == 'D') {
            nb++;
        }
    }
    double* data = malloc(nb * sizeof(double));
    char* trous = malloc(nb * sizeof(char));
    int index = 0;
    for (int i = 0; i < table->tailleMax; i++) {
        if (table->trous[i] == 'D') {
            data[index] = table->data[i];
            trous[index] = 'D';
            index++;
        }
    }
    free(table->data);
    free(table->trous);
    table->data = data;
    table->trous = trous;
    table->tailleMax = nb;
    table->nbElements = nb;
}

void swap(TableauATrous* table,int i, int j) {
    // Echange deux élements du tableau donnés par leur indice.
    double tmpData = table->data[i];
    char tmpTrou = table->trous[i];
    table->data[i] = table->data[j];
    table->trous[i] = table->trous[j];
    table->data[j] = tmpData;
    table->trous[j] = tmpTrou;
}

void bubble_sort(TableauATrous* table) {
   // Fonction qui trie le tableau de double (ordre croissant) en utilisant l’algorithme du bubble-sort. Les cases vides (T) restent ou elles sont.
    for (int i = 0; i < table->tailleMax; i++) {
        for (int j = 0; j < table->tailleMax - 1; j++) {
            if (table->trous[j] == 'D' && table->trous[j + 1] == 'D') {
                if (table->data[j] > table->data[j + 1]) {
                    swap(table, j, j + 1);
                }
            }
        }
    }

}



