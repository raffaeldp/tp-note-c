//
// Created by Raffael on 01/02/2023.
//

#ifndef PROG_C_CLION_TAB_TROUS_H
#define PROG_C_CLION_TAB_TROUS_H

    #include <stdbool.h>
    #include <stdio.h>
    #include <assert.h>
    #include <malloc.h>

    typedef struct TableauATrous {
        double *data;
        char *trous;
        int tailleMax;
        int nbElements;
        int indicePremiereDonnee;
    } TableauATrous;


    TableauATrous* create(int szm);
    void destroy(TableauATrous* table);
    bool is_empty(TableauATrous* table);
    int is_full(TableauATrous* table);
    int get_first_index(TableauATrous* table);
    int get_next_index(TableauATrous* table, int index);
    void print(TableauATrous* table);
    void clear(TableauATrous* table);
    int insert(TableauATrous* table, double val);
    double get_value(TableauATrous* table, int index);
    void set_value(TableauATrous* table, int index, double value);
    int release(TableauATrous* table, int index);
    int find_first_value(TableauATrous* table, double val);
    int find_nb_occurences(TableauATrous* table, double value);
    void shrink(TableauATrous* table);
    void swap(TableauATrous* table,int i, int j);
    void bubble_sort(TableauATrous* table);
#endif //PROG_C_CLION_TAB_TROUS_H
